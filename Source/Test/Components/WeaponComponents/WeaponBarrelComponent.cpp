
#include "WeaponBarrelComponent.h"
#include "../../TestTypes.h"
#include "DrawDebugHelpers.h"
#include "../Plugins/FX/Niagara/Source/Niagara/Public/NiagaraFunctionLibrary.h"
#include "../Plugins/FX/Niagara/Source/Niagara/Public/NiagaraComponent.h"

void UWeaponBarrelComponent::Shot(FVector ShotStart, FVector ShotDirection, AController* Instigator, AActor* DamageCauser)
{


	FVector ShotEnd = ShotStart + FireRange * ShotDirection;
	FVector MuzzleLocation = GetComponentLocation();
	UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), MuzzleFlashFX, MuzzleLocation, GetComponentRotation());
	
	FHitResult ShotResult;
	FCollisionQueryParams  QueryParams;
	QueryParams.AddIgnoredActor(DamageCauser);
	if (GetWorld()->LineTraceSingleByChannel(ShotResult, ShotStart, ShotEnd, ECC_Bullet,QueryParams))
	{
		ShotEnd = ShotResult.ImpactPoint;
		//DrawDebugSphere(GetWorld(), ShotEnd, 10.0f, 24, FColor::Red, false, 1.0f);
		AActor* HitActor = ShotResult.GetActor();
		HitActor->TakeDamage(DamageAmount, FDamageEvent{}, Instigator, DamageCauser);

	}
	UNiagaraComponent* TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), TraceFX, MuzzleLocation, GetComponentRotation());
	TraceFXComponent->SetVectorParameter(FXParamTraceEnd, ShotEnd);
	//DrawDebugLine(GetWorld(), MuzzleLocation, ShotEnd, FColor::Red, false, 1.0f, 0, 3.0f);
}
