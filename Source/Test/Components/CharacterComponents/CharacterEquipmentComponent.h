// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UObject/WeakObjectPtrTemplates.h"
#include "CharacterEquipmentComponent.generated.h"

class ARangeWeaponItem;

UENUM(BlueprintType)
enum class EEquippedITemType : uint8
{
	Pistol,
	Rifle,
	None
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TEST_API UCharacterEquipmentComponent : public UActorComponent
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loadout")
	TSubclassOf<ARangeWeaponItem> SideArmClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	ARangeWeaponItem* CurrentEquippedWeapon;
private:
	void CreateLoadout();
	
	FName CharacterWeaponSocket = FName("WeaponSocket");
	
	TWeakObjectPtr<ACharacter>  CurrentCharacter;
public:
	UFUNCTION(BlueprintCallable)
	EEquippedITemType GetCurrentEquippedItemType() const;

	UFUNCTION(BlueprintCallable)
	ARangeWeaponItem* GetCurrentRangeWeapon() const;


	void AddEquipmentItem(const TSubclassOf<class AEquipableItem> EquipableItemClass);
};
