
#include "CharacterEquipmentComponent.h"
#include "GameFramework/Character.h"
#include "Test/Actors/Equipment/Weapons/RangeWeaponItem.h"
#include "Test/Actors/Equipment/EquipableItem.h"


// Called when the game starts
void UCharacterEquipmentComponent::BeginPlay()
{
	Super::BeginPlay();
	checkf(GetOwner()->IsA<ACharacter>(),
		TEXT("UCharacterEquipmentComponent::BeginPlay() can be used only with ACharacter"));
	CurrentCharacter = StaticCast<ACharacter*>(this->GetOwner());
	CreateLoadout();
}

EEquippedITemType UCharacterEquipmentComponent::GetCurrentEquippedItemType() const
{
	EEquippedITemType Result = EEquippedITemType::None;
	if (IsValid(CurrentEquippedWeapon))
	{
		Result = CurrentEquippedWeapon->GetItemType();
	}
	return Result;
}

ARangeWeaponItem* UCharacterEquipmentComponent::GetCurrentRangeWeapon() const
{
	return CurrentEquippedWeapon;
}

void UCharacterEquipmentComponent::AddEquipmentItem(const TSubclassOf<AEquipableItem> EquipableItemClass)
{
	ARangeWeaponItem* RangeWeaponObject = Cast<ARangeWeaponItem>(EquipableItemClass->GetDefaultObject());
	if (!IsValid(RangeWeaponObject))
		return;

	//AmunitionArray[(uint32)RangeWeaponObject->GetAmmoType()] += RangeWeaponObject->GetMaxAmmo();
	//if(IsValid(CurrentEquippedWeapon))
	//OnCurrentWeaponChanged(CurrentEquippedWeapon->GetAmmo());
}



void UCharacterEquipmentComponent::CreateLoadout()
{
	if(!IsValid(SideArmClass))
	{
		return;
	}
	CurrentEquippedWeapon = GetWorld()->SpawnActor<ARangeWeaponItem>(SideArmClass);
	CurrentEquippedWeapon->AttachToComponent(CurrentCharacter.Get()->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, CharacterWeaponSocket);
	CurrentEquippedWeapon->SetOwner(CurrentCharacter.Get());
}

