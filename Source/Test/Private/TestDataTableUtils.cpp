// Fill out your copyright notice in the Description page of Project Settings.


#include "TestDataTableUtils.h"



FWeaponTableRow* TestDataTableUtils::FindWeaponData(FName WeaponID)
{
	static const FString contextString(TEXT("Find Weapon Data"));
	UDataTable* weaponDataTable = LoadObject<UDataTable>(nullptr, TEXT("Game/Data/DataTables/DT_WeaponList.DT_WeaponList"));
	if (weaponDataTable == nullptr)
		return nullptr;
	return weaponDataTable->FindRow<FWeaponTableRow>(WeaponID,contextString);
}
