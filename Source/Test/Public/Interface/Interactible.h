#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Test/TestCharacter.h"
#include "Interactible.generated.h"


UINTERFACE(MinimalAPI)
class UInteractible:public UInterface
{
	GENERATED_BODY()
};

class TEST_API IInteractible
{
	GENERATED_BODY()
public:
	virtual void Interact(ATestCharacter* Char) PURE_VIRTUAL(IInteractible::Interact,);
	virtual FName GetActionEventName() const PURE_VIRTUAL(IInteractible::GetActionIventName, return FName(NAME_None); );
};
