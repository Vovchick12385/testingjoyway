// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <Inventory/InventoryItem.h>

namespace TestDataTableUtils
{
	FWeaponTableRow* FindWeaponData(FName WeaponID);
}