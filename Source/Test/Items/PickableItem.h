#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Public/Interface/Interactible.h"
#include "PickableItem.generated.h"

UCLASS(Abstract, NotBlueprintable)
class TEST_API APickableItem : public AActor , public IInteractible
{
	GENERATED_BODY()
public:
	const FName& GetDataTableID() const;
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item")
	FName DataTableID = NAME_None;
};

