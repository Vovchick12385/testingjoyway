#pragma once

#define ECC_Bullet ECC_GameTraceChannel2

const FName FXParamTraceEnd = FName("TraceEnd");

const FName SocketWeaponForeGrip = FName("ForeGripSocket");
const FName SectionMontageReloadEnd = FName("ReloadEnd");

const FName ActionInteract = FName("Interact");
