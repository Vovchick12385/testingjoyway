// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Test/Components/CharacterComponents/CharacterEquipmentComponent.h"
#include "EquipableItem.generated.h"



UCLASS(Abstract, NotBlueprintable)
class TEST_API AEquipableItem : public AActor
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	EEquippedITemType GetItemType() const;
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponType")
	EEquippedITemType ItemType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item")
	FName DataTableID = NAME_None;
};
