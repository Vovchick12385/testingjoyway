// Fill out your copyright notice in the Description page of Project Settings.


#include "RangeWeaponItem.h"
#include "GameFramework/Character.h"
#include "Test/Components/WeaponComponents/WeaponBarrelComponent.h"
#include "Test/TestTypes.h"

ARangeWeaponItem::ARangeWeaponItem()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("WeaponRoot"));
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetupAttachment(RootComponent);

	WeaponBarrel = CreateDefaultSubobject<UWeaponBarrelComponent>(TEXT("WeaponBarrel"));
	WeaponBarrel->SetupAttachment(WeaponMesh, MuzzleSocket);
}

void ARangeWeaponItem::StartFire()
{
	MakeShot();
	if (WeaponFireMode == EWeaponFireMode::FullAuto)
	{
		GetWorld()->GetTimerManager().ClearTimer(ShotTimer);
		GetWorld()->GetTimerManager().SetTimer(ShotTimer, this, &ARangeWeaponItem::MakeShot, GetShotTimerInterval(), true);
	}
	
}

void ARangeWeaponItem::StopFire()
{
	GetWorld()->GetTimerManager().ClearTimer(ShotTimer);
}

FTransform ARangeWeaponItem::GetForeGripTransform() const
{
	return WeaponMesh->GetSocketTransform(SocketWeaponForeGrip);
}

float ARangeWeaponItem::PlayAnimMontage(UAnimMontage* Montage)
{
	UAnimInstance* WeaponAnimInstance = WeaponMesh->GetAnimInstance();

	return WeaponAnimInstance->Montage_Play(Montage);
}

float ARangeWeaponItem::GetShotTimerInterval()
{
	return 60/RateOfFire;
}

void ARangeWeaponItem::MakeShot()
{
	ACharacter* CharacterOwner = StaticCast<ACharacter*>(GetOwner());
	APlayerController* Controller = CharacterOwner->GetController<APlayerController>();
	FVector ShotStart;
	CharacterOwner->PlayAnimMontage(CharacterFireMontage);
	PlayAnimMontage(WeaponFireMontage);
	FRotator ShotDirectionRotation;
	if (IsValid(Controller))
	{
		Controller->GetActorEyesViewPoint(ShotStart, ShotDirectionRotation);
	}

	FVector ShotDirection = ShotDirectionRotation.RotateVector(FVector::ForwardVector);
	WeaponBarrel->Shot(ShotStart, ShotDirection, Controller, GetOwner());
}
