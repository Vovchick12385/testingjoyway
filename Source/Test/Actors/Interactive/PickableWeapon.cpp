// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Interactive/PickableWeapon.h"
#include "Test/TestTypes.h"
#include "Test/Public/TestDataTableUtils.h"
#include "Test/TestCharacter.h"

APickableWeapon::APickableWeapon()
{
	WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon Mesh"));
	SetRootComponent(WeaponMesh);
}

void APickableWeapon::Interact(ATestCharacter* Character)
{
	FWeaponTableRow* WeaponRow = TestDataTableUtils::FindWeaponData(DataTableID);
	if (WeaponRow)
	{
		Character->AddEquipmentItem(WeaponRow->EquipableItem);
		Destroy();
	}
}

FName APickableWeapon::GetActionEventName() const
{
	return ActionInteract;
}
